# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("../camera_usb.gni")

import("//build/ohos.gni")
import("../../../../hdf_core/adapter/uhdf2/uhdf.gni")

config("camhdi_impl_config") {
  visibility = [ ":*" ]
  cflags = [
    "-DGST_DISABLE_DEPRECATED",
    "-DHAVE_CONFIG_H",
  ]

  ldflags = [ "-Wl" ]

  if (enable_camera_device_utest) {
    cflags += [
      "-fprofile-arcs",
      "-ftest-coverage",
    ]

    ldflags += [ "--coverage" ]
  }
}

host_sources = [
  "$camera_path_usb/../interfaces/hdi_ipc/camera_host_driver.cpp",
  "$camera_path_usb/hdi_impl/src/camera_device/camera_device_impl.cpp",
  "$camera_path_usb/hdi_impl/src/camera_dump.cpp",
  "$camera_path_usb/hdi_impl/src/camera_host/camera_host_config.cpp",
  "$camera_path_usb/hdi_impl/src/camera_host/camera_host_impl.cpp",
  "$camera_path_usb/hdi_impl/src/offline_stream_operator/offline_stream.cpp",
  "$camera_path_usb/hdi_impl/src/offline_stream_operator/offline_stream_operator.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/capture_message.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/capture_request.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_base.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_operator.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_post_view.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_preview.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_statistics.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_still_capture.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_tunnel/standard/stream_tunnel.cpp",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_video.cpp",
]

host_includes = [
  "$camera_path_usb/../interfaces/include",
  "$camera_path_usb/../interfaces/hdi_ipc",
  "$camera_path_usb/../interfaces/hdi_ipc/utils/include",
  "$camera_path_usb/../interfaces/hdi_ipc/callback/host/include",
  "$camera_path_usb/../interfaces/hdi_ipc/callback/device/include",
  "$camera_path_usb/../interfaces/hdi_ipc/callback/operator/include",
  "$camera_path_vdi/include",
  "$camera_path_usb/hdi_impl",
  "$camera_path_vdi/metadata_manager/include",
  "$camera_path_vdi/utils/watchdog",
  "$camera_path_usb/hdi_impl/include",
  "$camera_path_usb/hdi_impl/include/camera_host",
  "$camera_path_usb/hdi_impl/include/camera_device",
  "$camera_path_usb/hdi_impl/include/stream_operator",
  "$camera_path_usb/hdi_impl/src/stream_operator/stream_tunnel/standard",
  "$camera_path_usb/hdi_impl/include/offline_stream_operator",
  "$camera_path_vdi/device_manager/include",
  "$camera_path_vdi/buffer_manager/src/buffer_adapter/standard",
  "$camera_path_vdi/utils/event",
  "$camera_path_usb/../../display/interfaces/include",

  #producer
  "$camera_path_vdi/pipeline_core/utils",
  "$camera_path_vdi/pipeline_core/include",
  "$camera_path_vdi/pipeline_core/host_stream/include",
  "$camera_path_vdi/pipeline_core/nodes/include",
  "$camera_path_vdi/pipeline_core/nodes/src/node_base",
  "$camera_path_vdi/pipeline_core/nodes/src/dummy_node",
  "$camera_path_vdi/pipeline_core/pipeline_impl/include",
  "$camera_path_vdi/pipeline_core/pipeline_impl/src",
  "$camera_path_vdi/pipeline_core/pipeline_impl/src/builder",
  "$camera_path_vdi/pipeline_core/pipeline_impl/src/dispatcher",
  "$camera_path_vdi/pipeline_core/pipeline_impl/src/parser",
  "$camera_path_vdi/pipeline_core/pipeline_impl/src/strategy",
  "$camera_path_vdi/pipeline_core/ipp/include",
]

ohos_shared_library("camera_daemon") {
  output_extension = "so"
  sources = host_sources
  include_dirs = host_includes
  include_dirs += [ "//third_party/libdrm" ]

  deps = [
    "$camera_path_usb/buffer_manager:camera_buffer_manager",
    "$camera_path_usb/device_manager:camera_device_manager",
    "$camera_path_usb/metadata_manager:camera_metadata_manager",
    "$camera_path_usb/pipeline_core:camera_pipeline_core",
    "$camera_path_usb/utils:camera_utils",
  ]

  defines = []
  if (enable_camera_device_utest) {
    defines += [ "CAMERA_DEVICE_UTEST" ]
  }
  if (use_hitrace) {
    defines += [ "HITRACE_LOG_ENABLED" ]
  }

  external_deps = [
    "c_utils:utils",
    "drivers_interface_camera:libcamera_stub_1.0",
    "drivers_interface_camera:metadata",
    "graphic_chipsetsdk:surface",
    "hdf_core:libhdf_host",
    "hdf_core:libhdf_ipc_adapter",
    "hdf_core:libhdf_utils",
    "hdf_core:libhdi",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
  ]
  if (use_hitrace) {
    external_deps += [ "hitrace_native:libhitracechain" ]
  }

  shlib_type = "hdi"
  public_configs = [ ":camhdi_impl_config" ]
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_static_library("camera_daemon_static") {
  sources = host_sources
  include_dirs = host_includes
  include_dirs += [ "//third_party/libdrm" ]

  deps = [
    "$camera_path_usb/buffer_manager:camera_buffer_manager",
    "$camera_path_usb/device_manager:camera_device_manager",
    "$camera_path_usb/metadata_manager:camera_metadata_manager",
    "$camera_path_usb/pipeline_core:camera_pipeline_core",
    "$camera_path_usb/utils:camera_utils",
  ]

  defines = []
  if (enable_camera_device_utest) {
    defines += [ "CAMERA_DEVICE_UTEST" ]
  }
  if (use_hitrace) {
    defines += [ "HITRACE_LOG_ENABLED" ]
  }

  external_deps = [
    "c_utils:utils",
    "drivers_interface_camera:libcamera_stub_1.0",
    "drivers_interface_camera:metadata",
    "graphic_chipsetsdk:surface",
    "hdf_core:libhdf_host",
    "hdf_core:libhdf_ipc_adapter",
    "hdf_core:libhdf_utils",
    "hdf_core:libhdi",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
  ]
  if (use_hitrace) {
    external_deps += [ "hitrace_native:libhitracechain" ]
  }

  public_configs = [ ":camhdi_impl_config" ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}
